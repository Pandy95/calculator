﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double total;
            char oper;

            Console.WriteLine("Оберiть дiю:");
            oper = Convert.ToChar(Console.ReadLine());

            Console.WriteLine("Введiть перше число:");
            a = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введiть друге число:");
            b = Convert.ToDouble(Console.ReadLine());


            if (oper == '+')
            {
                total = a + b;
                Console.WriteLine("Cума " + a + " та " + b + " Дорiвнюе " + total + ".");
            }

            else if (oper == '-')
            {
                total = a - b;
                Console.WriteLine("Вiднiмання " + a + " та " + b + " Дорiвнюе " + total + ".");
            }

            else if (oper == '*')
            {
                total = a * b;
                Console.WriteLine("Множення " + a + " на " + b + " Дорiвнюе " + total + ".");
            }

            else if (oper == '/')
            {
                total = a / b;
                Console.WriteLine("Дiлення " + a + " на " + b + " Дорiвнюе " + total + ".");
            }
            else
            {
                Console.WriteLine("Невiдомий оператор.");
            }
            return;
        }
    }
}
